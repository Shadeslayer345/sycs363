package howardedu.sycs363.spring15.lab4;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class to hold phone contacts in a Hashmap for quick retrieval
 */
public class PhoneDirectory {
    private Map<String, String> directoryMap = new HashMap<String, String>();
    final private static Charset ENCODING = StandardCharsets.UTF_8;

    /**
     * Creates a phone directory object with data from a default file
     * @throws IOException Possible error when reading from file.
     * @constructor
     */
    public PhoneDirectory() {
        Path inPath = Paths.get("infile.txt");
        try (Scanner scanner = new Scanner(inPath,
                    ENCODING.name())) {
            while(scanner.hasNextLine()) {
                String[] info = scanner.nextLine().split(",");
                directoryMap.put(info[0].trim(), info[1].trim());
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
     }

    /**
     * Creates a phone directory object with data from.
     * @param  phoneDirectoyFile path to the file to use
     * @throws IOException possible error when reading from file
     */
    public PhoneDirectory(String phoneDirectoyFile) {
        Path inPath = Paths.get(phoneDirectoyFile);
        try (Scanner scanner = new Scanner(inPath,
                    StandardCharsets.UTF_8.name())) {
            while(scanner.hasNextLine()) {
                String[] info = scanner.nextLine().split(",");
                directoryMap.put(info[0].trim(), info[1]);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());        }
    }

    /**
     * Returns the contact information for the specified person.
     * @param  personName the key value for lookup
     * @return a formated string with the contact name and number
     */
    public String lookup(String personName) {
        return personName + ", " + directoryMap.get(personName);
    }

    /**
     * Adds a new person or changes the current contact name to the user
     *         specified one for a specific phone number.
     * @param name key value for lookup
     * @param phoneNumber value for insertion
     */
    public void addorChangeEntry(String name, String phoneNumber) {
        if (directoryMap.containsValue(phoneNumber)) {
            ArrayList<String> repeats = new ArrayList<String>();
            for (String key : directoryMap.keySet()) {
                if (directoryMap.get(key).equals(phoneNumber)) {
                    repeats.add(key);
                }
            } for (String tempName : repeats) {
                directoryMap.remove(tempName);
            }
        }
        directoryMap.put(name, phoneNumber);
    }

    /**
     * Removed a contact form the phone directory.
     * @param name the key value for lookup
     */
    public void deleteEntry(String name) {
        directoryMap.remove(name);
    }

    /**
     * Outputs the phone directory from memory to a file
     * @throws IOException possible error when creating and writing to file
     */
    public void write() {
        Path outPath = Paths.get("./outfile.txt");
        try (BufferedWriter writer = Files.newBufferedWriter(outPath,
                    ENCODING)) {
            for (Map.Entry<String, String> contact : directoryMap.entrySet()) {
                writer.write(contact.getKey() + ", " + contact.getValue() +
                        "\n");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
