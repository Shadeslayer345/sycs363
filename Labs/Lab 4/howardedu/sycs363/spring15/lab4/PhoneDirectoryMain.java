package howardedu.sycs363.spring15.lab4;

import java.lang.InterruptedException;
import java.util.Scanner;

public class PhoneDirectoryMain {
    public static void main(String[] args) {
        PhoneDirectory myContacts;
        System.out.println("LAB 4: HASHMAP IMPLEMENTATION");
        System.out.println("------------------------------");
        System.out.print("Do you have your own readable file (.txt) or " +
                "would you like to use the default? (Y/N): ");

        Scanner input = new Scanner(System.in);
        String choiceOne = input.nextLine();
        if (choiceOne.equalsIgnoreCase("Y")) {
            System.out.print("\n Ok! Please enter the path to your file: ");
            String path =  input.nextLine();

            myContacts = new PhoneDirectory(path);
        } else {
            myContacts = new PhoneDirectory();
        }

        System.out.println("\n What would you like to do: ");
        System.out.print("1 - Look up a contact \n2 - Add a contact \n" +
                "3 - Delete a contact \n4 - Change a contact's phone number" +
                "\n5 - Try it all\nChoice: ");

        int choiceTwo = input.nextInt();
        switch (choiceTwo) {
            case 1: ; find(myContacts, input); break;
            case 2: ; add(myContacts, input); break;
            case 3: ; delete(myContacts, input); break;
            case 4: ; add(myContacts, input); break;
            default:  auto(myContacts); break;

        }
        myContacts.write();
    }

    /**
     * Performs a lookup on the phone directory for a name.
     * @param contacts phone directory to search
     * @param read keyboard input reader
     */
    public static void find(PhoneDirectory contacts, Scanner read) {
        System.out.println("\n Enter the name of the person to lookup: ");
        String nameToFind = read.nextLine();

        System.out.println("Contact found! \n");
        contacts.lookup(nameToFind);
    }

    /**
     * Adds a new contact to the phone directory.
     * @param contacts phone directory to search
     * @param read keyboard input reader
     */
    public static void add(PhoneDirectory contacts, Scanner read) {
        System.out.println("\n Enter the name of the person to add/modify: ");
        String nameToAdd = read.nextLine();

        System.out.println("\n Enter the number to add for this contact: ");
        String numberToAdd = read.nextLine();

        contacts.addorChangeEntry(nameToAdd, numberToAdd);
        System.out.println("\n Contact added/modified");

    }

    /**
     * Removes a contact from the phone directory.
     * @param contacts phone directory to search
     * @param read keyboard input reader
     */
    public static void delete(PhoneDirectory contacts, Scanner read) {
        System.out.println("\n Enter the name of the person to delete: ");
        String nameToAdd = read.nextLine();

        System.out.println("\n Enter the number to add for this contact: ");
        String numberToAdd = read.nextLine();

        contacts.addorChangeEntry(nameToAdd, numberToAdd);
        System.out.println("\n Contact removed");
    }

    /**
     * Calls all methods from PhoneDirectory class for user.
     * @param contacts phone directory to read
     */
    public static void auto(PhoneDirectory contacts) {
        System.out.println("\n1. Looking up name Barry Harris");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println(contacts.lookup("Barry Harris") + "\n");

        System.out.println("2. Adding name, Ash Ketchum with number " +
                "111-111-1111.");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println("Looking up Ask Ketchum");
        contacts.addorChangeEntry("Ash Ketchum", "111-111-1111");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println(contacts.lookup("Ash Ketchum") + "\n");

        System.out.println("Check outfile.txt for results of 3, 4 and 5 \n");

        System.out.println("3. Replacing a phone number for a contact");
        System.out.print(contacts.lookup("Carter Carson") + " -> ");
        contacts.addorChangeEntry("Carter Carson", "972-217-0743");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.print(contacts.lookup("Carter Carson") + "\n");


        System.out.println("\n4. Adding a name with a number that already " +
                "exist.");
        System.out.print(contacts.lookup("Barry Harris") + " -> ");
        contacts.addorChangeEntry("New Person", "972-827-0384");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.print(contacts.lookup("New Person") + " \n");

        System.out.println("\n5. Removing a name from the directory");
        System.out.print(contacts.lookup("Ericka Slate") + " -> ");
        contacts.deleteEntry("Ericka Slate");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println("removed");

        System.out.println("\nFile written!");
    }
}
