package howardedu.sycs363.spring15.lab3;

public class IntegerArrayListMain {
    public static void main(String[] args) {
        System.out.println("Lab3");
        System.out.println("-------------------------------------\n");

        // Invoking default constructor for IntegerArrayList class
        IntegerArrayList listOne = new IntegerArrayList();

        // Testing array emptyness
        boolean popStatus = listOne.isEmpty();
        System.out.println("Array is empty: " + popStatus + "\n\n");

        // Adding multiple elements to array
        listOne.add(10);
        listOne.add(100);
        listOne.add(110);
        System.out.println("Starting IntegerArrayList 1:");
        System.out.print("[");
        for (int i = 0; i < 3; i++) {
            System.out.print(listOne.get(i) + " ");
        }
        System.out.println("]\n\n");

        // Replacing an element using the overloaded add method
        System.out.println("Original value at position 1: " + listOne.get(1));
        listOne.add(1, 200);
        System.out.println("New value at position 1: " + listOne.get(1) +
            "\n\n");


        // Adding an element in nonsequential order
        listOne.add(5,225);
        System.out.println("Added value in non-sequential order...");
        System.out.println("IntegerArrayList 1:");
        System.out.print("[");
        for (int i = 0; i < 6; i++) {
            System.out.print(listOne.get(i) + " ");
        }
        System.out.println("]\n\n");

        // Removing an element from array, sets value to 0
        System.out.println("Original value at position 0: " + listOne.get(0));
        int valueRemoved = listOne.remove(0);
        System.out.println("IntegerArrayList 1:");
        System.out.print("[");
        for (int i = 0; i < 5; i++) {
            System.out.print(listOne.get(i) + " ");
        }
        System.out.println("]\n\n");
        System.out.println("Value removed: " + valueRemoved + "\n\n\n");

        // Dynamically increasing size of array
        // Invoking parameter constructor for IntegerArrayList class
        IntegerArrayList listTwo = new IntegerArrayList(3);
        listTwo.add(1);
        listTwo.add(2);
        listTwo.add(3);
        listTwo.add(4);
        System.out.println("New value added to expanded array: " +
            listTwo.get(3));
    }
}
