import com.mongodb.*;

public class hello {
    public static void main(String[] args) {
        try {
            /** Connect */
            MongoClient client = new MongoClient("localhost", 27017);

            /** Get Database (or create it) */
            DB db = client.getDB("test");
            System.out.println("Connect successfully");

            /** Get table */
            DBCollection table = db.getCollection("users");

            /** Insert entry */
            BasicDBObject doc = new BasicDBObject();
            doc.put("name", "Barry Harris");
            doc.put("age", 19);
            table.insert(doc);

            /** Find and display */
            BasicDBObject search = new BasicDBObject();
            search.put("name", "Barry");

            DBCursor traverse = table.find(search);

            while (traverse.hasNext()) {
                System.out.println(traverse.next());
            }

            System.out.println("FINISHED");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
