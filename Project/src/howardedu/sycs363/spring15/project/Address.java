package howardedu.sycs363.spring15.project;

/**
 * Class for address specific information for a contact
 */
public class Address {
    private String streetName, city, state;
    private int houseNumber, zipCode;

    /**
     * Single parameter constructor for the Address class, uses parameters to
     *         set all data members.
     * @param  newStreetName new value for streetName data member
     * @param  newHouseNumber new value for houseNumber data member
     * @param  newCity new value for city data member
     * @param  newState new value for state data member
     * @param  newZipCode new value for zipCode data member
     * @constructor
     */
    public Address(String newStreetName, int newHouseNumber,
            String newCity, String newState, int newZipCode) {
        streetName = newStreetName;
        houseNumber = newHouseNumber;
        city = newCity;
        state = newState;
        zipCode = newZipCode;
    }

    /**
     * Allows user to modify the streetName data member.
     * @param newStreetName new value for the streetName data member
     */
    public void changeStreetName(String newStreetName) {
        streetName = newStreetName;
    }

    /**
     * Allows user to modify the houseNumber data member.
     * @param newHouseNumber new value for the houseNumber data member
     */
    public void changeHouseNumber(int newHouseNumber) {
        houseNumber = newHouseNumber;
    }

    /**
     * Allows user to modify the city data member.
     * @param newCity new value for the city data member
     */
    public void changeCity(String newCity) {
        city = newCity;
    }

    /**
     * Allows user to modify the state data member.
     * @param newState new value for the state data member.
     */
    public void changeState(String newState) {
        state = newState;
    }

    /**
     * Allows user to modify the zipCode data member.
     * @param newZipCode new value for the zipCode data member
     */
    public void changeZipCode(int newZipCode) {
        zipCode = newZipCode;
    }

    /**
     * Gives value for the streetname data member.
     * @return streetName data member
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Gives value for the houseNumber data member.
     * @return houseNumber data member
     */
    public int getHouseNumber() {
        return houseNumber;
    }

    /**
     * Gives value for the city data member.
     * @return city data member
     */
    public String getCity() {
        return city;
    }

    /**
     * Gives value for state data member.
     * @return state data member
     */
    public String getState() {
        return state;
    }

    /**
     * Gives value for the zipCode data member.
     * @return zipCode data member
     */
    public int getZipCode() {
        return zipCode;
    }

    /**
     * Outputs all data member to the console.
     */
    public void print() {
        System.out.println("\nAddress:");
        System.out.println(houseNumber + " " + streetName);
        System.out.println(city + ", " + state + " " + zipCode);
    }

    /**
     * Overloaded method to allow comparison of two Address objects
     * @param  otherAddress Address object used for comparison
     * @return three possible values
     *               0 - The two objects are equal
     *               1 - The object calling the function is greater
     *               -1 - The object calling the functino is less
     */
    public int compareTo(Address otherAddress) {
        if (zipCode == otherAddress.zipCode) {
            if (houseNumber > otherAddress.houseNumber) {
                return 1;
            } else if (houseNumber < otherAddress.houseNumber) {
                return -1;
            } else {
                return 0;
            }
        } else if (zipCode > otherAddress.zipCode) { 
            return 1;
        } else {
            return -1;
        }
    }
}
