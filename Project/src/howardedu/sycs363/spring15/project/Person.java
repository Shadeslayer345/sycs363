package howardedu.sycs363.spring15.project;

/**
 * Class for person specific information for a contact.
 */
public class Person {
    private String firstName, lastName, number;
    public Address address;

    /**
     * Single parameter constructor, parameters used to set all data members.
     * @param  newFirstName new value for the firstName data member
     * @param  newLastName new value for the lastname data memebr
     * @param  newNumber new value for the number data member
     * @param  newAddress new value for the address object data member
     * @constructor
     */
    public Person(String newFirstName, String newLastName, String newNumber,
        Address newAddress) {
        firstName = newFirstName;
        lastName = newLastName;
        number = newNumber;
        address = newAddress;
    }

    /**
     * Allows user to modify the newNumber data member.
     * @param newNumber new value for the newNumber
     */
    public void changeNumber(String newNumber) {
        number = newNumber;
    }

    /**
     * Allows user to modify a string data member of the address object
     *         data member.
     * @param option choice for which data member to modify
     * @param newDescriptor new value for data member of the address object
     *         data member
     */
    public void changeAddress(int option, String newDescriptor) {
        switch(option) {
            case 1: address.changeStreetName(newDescriptor);
            case 2: address.changeCity(newDescriptor);
            case 3: address.changeState(newDescriptor);
            default: return;
        }
    }

    /**
     * ALlows user to modify an integer data member of the address object
     *         data member.
     * @param option choice for which data member to modify
     * @param newDescriptor new value for data member of the address object
     *         data member
     */
    public void changeAddress(int option, int newDescriptor) {
        if (option == 1) {
            address.changeZipCode(newDescriptor);
        } else if (option == 2) {
            address.changeHouseNumber(newDescriptor);
        } else {
            return;
        }
    }

    /**
     * Allows user to modify the address object of the person.
     * @param newStreetName new value for streetName data member of address
     *         object
     * @param newHouseNumber new value for houseNumber data member of address
     *         object
     * @param newCity        new value for city data member of address object
     * @param newState       new value for state data member of address object
     * @param newZipCode     new value for zipCode data member of address object
     */
    public void changeAddress(String newStreetName, int newHouseNumber,
            String newCity, String newState, int newZipCode) {
        address.changeStreetName(newStreetName);
        address.changeHouseNumber(newHouseNumber);
        address.changeCity(newCity);
        address.changeState(newState);
        address.changeZipCode(newZipCode);
    }

    /**
     * Gives the value of the firstName data member.
     * @return firstName data member
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gives the value of the lastName data member.
     * @return lastName data member.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Gives the value of the number data member.
     * @return number data member
     */
    public String getNumber() {
        return number;
    }

    /**
     * Gives the value of the streetName data member of the address object data
     *         member.
     * @return streetName data member of the address object data member
     */
    public String getAddressStreetName() {
        return address.getStreetName();
    }

    /**
     * Gives the value of the housenumber data member of the address object data
     *         member.
     * @return housenumber data member of the address object data member
     */
    public int getAddressHouseNumber() {
        return address.getHouseNumber();
    }

    /**
     * Gives the value of the city data member of the address object data
     *         member.
     * @return city data member of the address object data member
     */
    public String getAddressCity() {
        return address.getCity();
    }

    /**
     * Gives the value of the state data member of the address object data
     *         member.
     * @return state data member of the address object data member
     */
    public String getAddressState() {
        return address.getState();
    }

    /**
     * Gives the value of the zipCode data member of the address object data
     *         member.
     * @return zipCode data member of the address object data member
     */
    public int getAddressZipCode() {
        return address.getZipCode();
    }

    /**
     * Outputs all data members to the console.
     */
    public void print() {
        System.out.println("\nContact:");
        System.out.println("\nPersonal");
        System.out.println(firstName + " " + lastName);
        System.out.println(number);
        address.print();
    }

    /**
     * Overloaded method that allows two Person Objects to be compared.
     * @param  otherPerson Person object used for comparison
     * @return Three values
     *               0 - The Person objects are equal
     *               1 - The Person oject calling the method is greater
     *               -1 - The Person oject calling the method is less
     */
    public int compareTo(Person otherPerson) {
        int personCompareVal = lastName.compareTo(otherPerson.lastName);
        if (personCompareVal > 0) {
            return 1;
        } else if (personCompareVal < 0) {
            return -1;
        } else {
            int addressCompareVal = address.compareTo(otherPerson.address);
            if (addressCompareVal > 0) {
                return 1;
            } else if (addressCompareVal < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
