package howardedu.sycs363.spring15.project;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Scanner;

public class SimpleAddressBook {

    private Map<String, Person> addressBook = new HashMap<String, Person>();
    final private static Charset ENCODING = StandardCharsets.UTF_8;

    /**
     * Creates a SimpleAddressBook object with data from a default file
     * @throws IOException Possible error when reading from file.
     * @constructor
     */
    public SimpleAddressBook() {
        Path inPath = Paths.get("infile.txt");
        try (Scanner scanner = new Scanner(inPath,
                    ENCODING.name())) {
            while(scanner.hasNextLine()) {
                String[] info = scanner.nextLine().split(",");
                Address tempAddress = new Address(info[4],
                        Integer.parseInt(info[5]), info[6], info[7],
                        Integer.parseInt(info[8]));
                Person tempPerson = new Person(info[1], info[2], info[3],
                        tempAddress);
                addressBook.put(info[0].trim(), tempPerson);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
     }

    /**
     * Creates a SimpleAddressBook object with data from.
     * @param  simpleaddressbookFile path to the file to use
     * @throws IOException possible error when reading from file
     */
    public SimpleAddressBook(String simpleaddressbookFile) {
        Path inPath = Paths.get(simpleaddressbookFile);
        try (Scanner scanner = new Scanner(inPath,
                    StandardCharsets.UTF_8.name())) {
            while(scanner.hasNextLine()) {
                String[] info = scanner.nextLine().split(",");
                Address tempAddress = new Address(info[4],
                        Integer.parseInt(info[5]), info[6], info[7],
                        Integer.parseInt(info[8]));
                Person tempPerson = new Person(info[1], info[2], info[3],
                        tempAddress);
                addressBook.put(info[0].trim(), tempPerson);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Inserts a new contact into the address book.
     * @param key a nickname for the contact, used a unique key
     * @param newContact Person object stored as value in hashmap
     */
    public void addContact(String key, Person newContact) {
        addressBook.put(key, newContact);
    }

    /**
     * Allows user to modify the Person object.
     * @param choice which data member the user would like to modify
     * @param contactName the key to use for look up
     * @param field new value for address object data member
     */
    public void editContact(int choice, String contactName, int field) {
        getContact(contactName).changeAddress(choice, field);
    }

    /**
     * Allows user to modify the Person object.
     * @param choice which data member the user would like to modify
     * @param contactName the key to use for look up
     * @param field new value for address object data member
     */
    public void editContact(int choice,String contactName, String field) {
        Person contact = getContact(contactName);
        if (choice == 4) {
            contact.changeNumber(field);
        } else {
            contact.changeAddress(choice, field);
        }
    }

    /**
     * Allows user to modify the Person object.
     * @param contactName    new value for Person object data member
     * @param newNumber      new value for Person object data member
     * @param newStreetName  new value for Person object data member
     * @param newHouseNumber new value for Person object data member
     * @param newCity        new value for Person object data member
     * @param newState       new value for Person object data member
     * @param newZipCode     new value for Person object data member
     */
    public void editContact(String contactName, String newNumber,
            String newStreetName, int newHouseNumber, String newCity,
            String newState, int newZipCode) {
        Person contact = getContact(contactName);

        contact.changeNumber(newNumber);
        contact.changeAddress(newStreetName, newHouseNumber, newCity,
                newState, newZipCode);
    }

    /**
     * Finds a contact in the address book.
     * @param  contactName key value to use for look up
     * @return The found person object
     */
    public Person getContact(String contactName) {
        return addressBook.get(contactName);
    }

    /**
     * Removes a contact from the address book.
     * @param  contactName key value to use for the look up
     */
    public void deleteContact(String contactName) {
        addressBook.remove(contactName);
    }

    /**
     * Outputs all contact information.
     * @param contactName key value to use for the lookup
     */
    public void printContact(String contactName) {
        getContact(contactName).print();
    }

    /**
     * Sorts the address book, either by last name of contact or zip code of
     *         contact's address.
     * @param choice which sorting method is preferred
     */
    private void sort(int choice) {
        if (choice == 1) {
           addressBook = sortByContact();
        } else {
           addressBook = sortByAddress();
        }
    }

    /**
     * Sorting the Hashmap by contact last name, utilizes treemap and comparator
     *      classes.
     * @return sorted address book
     */
    private TreeMap<String, Person> sortByContact() {
        ContactComparator contactComp = new ContactComparator(addressBook);
        TreeMap<String, Person> sortedAddressBook =
                new TreeMap<String, Person>(contactComp);
        sortedAddressBook.putAll(addressBook);
        return sortedAddressBook;
    }

    /**
     * Sorting the Hashmap by contact zip code, utilizes treemap and comparator
     *         classes.
     * @return sorted address book
     */
    private TreeMap<String, Person> sortByAddress() {
        AddressComparator addressComparator =
                new AddressComparator(addressBook);
        TreeMap<String, Person> sortedAddressBook =
                new TreeMap<String, Person>(addressComparator);
        sortedAddressBook.putAll(addressBook);
        return sortedAddressBook;
    }

    /**
     * Outputs all data in HashMap into a file or database. Utilizes a TreeMap
     *         with a comparator to sort the address book, meaning the address
     *         book can only be written to a file or database once because the
     *         inherent data structure (Map) will be changed.
     * @param choice which source the user would like to write the data too
     */
    public void writeOut(int choice) {
        sort(1);
        if (choice == 1) {
            Path outPath = Paths.get("./outfile.txt");
            try (BufferedWriter writer = Files.newBufferedWriter(outPath,
                        ENCODING)) {
                for (Map.Entry<String, Person> entry : addressBook.entrySet()) {
                    Person contact = entry.getValue();
                    writer.write(entry.getKey() + "," +
                            contact.getFirstName() + "," +
                            contact.getLastName() + "," +
                            contact.getNumber() + "," +
                            contact.getAddressStreetName() + "," +
                            contact.getAddressHouseNumber() + "," +
                            contact.getAddressCity() + "," +
                            contact.getAddressState() + "," +
                            contact.getAddressZipCode() +
                            "\n");
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            System.out.println("Data base support coming soon!");
        }
    }
}

/**
 * Helper class to create TreeMap sorted by lastname.
 */
class ContactComparator implements Comparator<String> {
    Map<String, Person> map;

    /**
     * Single parameter constructor
     * @param  base map to be used for sorting
     * @constructor
     */
    public ContactComparator(Map<String, Person> base) {
        this.map = base;
    }

    /**
     * Compares TreeMap entries based on Person compareTo.
     * @param  personA person object
     * @param  personB person object
     * @return Three values
     *               1 - a < b
     *               0 or -1 - a < b
     */
    public int compare(String a, String b) {
        if (map.get(a).compareTo(map.get(b)) <= 0) {
            return -1;
        } else {
            return 1;
        }
    }
}

/**
 * Helper class to create TreeMap sorted by lastname.
 */
class AddressComparator implements Comparator<String> {
    Map<String, Person> map;

    /**
     * Single parameter constructor
     * @param  base map to be used for sorting
     * @constructor
     */
    public AddressComparator(Map<String, Person> base) {
        this.map = base;
    }

    /**
     * Compares TreeMap entries based on Person compareTo.
     * @param  personA person object
     * @param  personB person object
     * @return Three values
     *               1 - personA < personB
     *               0 or -1 - personA < personB
     */
    public int compare(String a, String b) {
        if (map.get(a).address.compareTo(map.get(b).address) <= 0) {
            return -1;
        } else {
            return 1;
        }
    }
}
