package howardedu.sycs363.spring15.project;

import static org.junit.Assert.*;
import org.junit.*;

import org.junit.Test;

public class AddressTest {

    private Address address;
    
    @Before
    public void beforeEach() {
        address = new Address("Thistle Green Ln", 906, "Duncanville", "Texas",
                75137);
    }
    
    @Test
    public void testAddress() {
        assertEquals("Failure, object not initialized with correct data",
                    "Thistle Green Ln", address.getStreetName());
        assertEquals("Failure, object not initialized with correct data",
                906, address.getHouseNumber());
        assertEquals("Failure, object not initialized with correct data",
                "Duncanville", address.getCity());
        assertEquals("Failure, object not initialized with correct data",
                "Texas", address.getState());
        assertEquals("Failure, object not initialized with correct data",
                75137, address.getZipCode());
    }

    @Test
    public void testChangeStreetName() {
        address.changeStreetName("Green Briar");
        assertEquals("Failure, street name not changed correctly",
                "Green Briar", address.getStreetName());
    }
    
    @Test
    public void testChangeHouseNumber() {
        address.changeHouseNumber(775);
        assertEquals("Failure, house number not changed correctly", 775,
                address.getHouseNumber());
    }

    @Test
    public void testChangeCity() {
        address.changeCity("Cedar Hill");
        assertEquals("Failure, city not changed correctly", "Cedar Hill",
                address.getCity());
    }

    @Test
    public void testChangeState() {
        address.changeState("Florida");
        assertEquals("Failure, state not changed correctly", "Florida",
                address.getState());
    }

    @Test
    public void testChangeZipCode() {
        address.changeZipCode(75116);
        assertEquals("Failure, zip code not changed correctly", 75116,
                address.getZipCode());
    }

    @Test
    public void testCompareTo() {
        Address address2 = new Address("Bliss Ln", 906, "Duncanville", "Texas",
                75116);
        Address address3 = new Address("Bliss Ln", 906, "Duncanville", "Texas",
                75147);
        Address address4 = new Address("Bliss Ln", 905, "Duncanville", "Texas",
                75137);
        Address address5 = new Address("Bliss Ln", 907, "Duncanville", "Texas",
                75137);
        
        assertEquals("Failure, compare method not implemented correctly", 1,
                address.compareTo(address2));
        assertEquals("Failure, compare method not implemented correctly", -1, 
                address.compareTo(address3));
        assertEquals("Failure, compare method not implemented correctly", 1,
                address.compareTo(address4));
        assertEquals("Failure, compare method not implemented correctly", -1,
                address.compareTo(address5));
        assertEquals("Failure, compare method not implemented correctly", 0,
                address.compareTo(address));
    }
}
