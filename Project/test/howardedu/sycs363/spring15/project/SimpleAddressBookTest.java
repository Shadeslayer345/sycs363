package howardedu.sycs363.spring15.project;

import static org.junit.Assert.*;
import org.junit.*;

import org.junit.Test;

public class SimpleAddressBookTest {
    
    SimpleAddressBook addressBook;
    
    @Before
    public void beforeEach() {
        addressBook = new SimpleAddressBook();
    }
    
    @Test
    public void testSimpleAddressBook() {
        Address address = new Address("Thistle Green", 906, "Duncanville",
                "Texas", 75137);
        Person person = new Person("Barry", "Harris", "9999", address);
        assertEquals("Failure, address book not initialized correctly", 0,
                person.compareTo(addressBook.getContact("Hiccup")));
    }

    @Test
    public void testSimpleAddressBookString() {
        addressBook = new SimpleAddressBook("./infile.txt");
        Address address = new Address("Thistle Green", 906, "Duncanville",
                "Texas", 75137);
        Person person = new Person("Barry", "Harris", "9999", address);
        assertEquals("Failure, address book not initialized correctly", 0,
                person.compareTo(addressBook.getContact("Hiccup")));
    }
    
    @Test
    public void testAddContact() {
        Address address = new Address("Hip", 9, "Whip", "Crack", 75137);
        Person person = new Person("Byron", "Cream", "9", address);
        addressBook.addContact("EH", person);
        
        assertEquals("Failure, contact not added correctly", 0,
                person.compareTo(addressBook.getContact("EH")));
    }
    
    @Test
    public void testwriteOut() {
        Address address = new Address("Hip", 9, "Whip", "Crack", 75137);
        Person person = new Person("Byron", "Cream", "9", address);
        addressBook.addContact("EH", person);
        
        addressBook.writeOut(1);
    }
}
