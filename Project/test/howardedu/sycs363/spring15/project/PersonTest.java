package howardedu.sycs363.spring15.project;

import static org.junit.Assert.*;

import org.junit.*;

public class PersonTest {
    private Address address;
    private Person person;
    
    @Before
    public void beforeEach() {
        address = new Address("Thistle Green Ln", 906, "Duncanville", "Texas",
                75137);
        person = new Person("Barry", "Harris", "999999", address); 
    }
    
    @Test
    public void testPerson() {
        assertEquals("Failure, person object not initialized correctly",
                "Barry", person.getFirstName());
        assertEquals("Failure, person object not initialized correctly",
                "Harris", person.getLastName());
        assertEquals("Failure, person object not initialized correctly",
                "999999", person.getNumber());
        assertEquals("Failure, person object not initialized correctly",
                "Thistle Green Ln", person.address.getStreetName());
        assertEquals("Failure, person object not initialized correctly",
                906, person.address.getHouseNumber());
        assertEquals("Failure, person object not initialized correctly",
                "Duncanville", person.address.getCity());
        assertEquals("Failure, person object not initialized correctly",
                "Texas", person.address.getState());
        assertEquals("Failure, person object not initialized correctly",
                75137, person.address.getZipCode());
    }

    @Test
    public void testChangeNumber() {
        person.changeNumber("1234");
        assertEquals("Failure, number not changed correctly", "1234",
                person.getNumber());
    }

    @Test
    public void testChangeAddressIntString() {
        person.changeAddress(1, "Green Briar");
        person.changeAddress(2, "Pheonix");
        person.changeAddress(3, "Florida");
        assertEquals("Failure, street name not changed correctly",
                "Green Briar", person.address.getStreetName());
        assertEquals("Failure, city not changed correctly", "Pheonix",
                person.address.getCity());
        assertEquals("Failure, state not changed correctly", "Florida",
                person.address.getState());
    
    }

    @Test
    public void testChangeAddressIntInt() {
        person.changeAddress(2, 907);
        person.changeAddress(1, 75116);
        assertEquals("Failure, house number not changed correctly", 907,
                person.address.getHouseNumber());
        assertEquals("Failure, house number not changed correctly", 75116,
                person.address.getZipCode());
    }

    @Test
    public void testChangeAddressStringIntStringStringInt() {
        person.changeAddress("Green Briar", 907, "Pheonix", "Florida", 75116);
        assertEquals("Failure, street name not changed correctly",
                "Green Briar", person.address.getStreetName());
        assertEquals("Failure, city not changed correctly", "Pheonix",
                person.address.getCity());
        assertEquals("Failure, state not changed correctly", "Florida",
                person.address.getState());
        assertEquals("Failure, house number not changed correctly", 907,
                person.address.getHouseNumber());
        assertEquals("Failure, house number not changed correctly", 75116,
                person.address.getZipCode());
    }

    @Test
    public void testCompareTo() {
        Address address4 = new Address("T", 9, "H", "S", 75116);
        Address address5 = new Address("T", 9, "H", "S", 75147);
        Address address6 = new Address("T", 8, "H", "S", 75137);
        Address address7 = new Address("T", 907, "H", "S", 75137);
        
        Person person2 = new Person("Barry", "Carol", "999", address);
        Person person3 = new Person("Barry", "Kenneth", "999", address);
        Person person4 = new Person("Barry", "Harris", "999", address4);
        Person person5 = new Person("Barry", "Harris", "999", address5);
        Person person6 = new Person("Barry", "Harris", "999", address6);
        Person person7 = new Person("Barry", "Harris", "999", address7);
        
        assertEquals("Failure, compare method failed", 1,
                person.compareTo(person2));
        assertEquals("Failure, compare method failed", -1,
                person.compareTo(person3));
        assertEquals("Failure, compare method failed", 1,
                person.compareTo(person4));
        assertEquals("Failure, compare method failed", -1,
                person.compareTo(person5));
        assertEquals("Failure, compare method failed", 1,
                person.compareTo(person6));
        assertEquals("Failure, compare method failed", -1,
                person.compareTo(person7));
        assertEquals("Failure, compare method failed", 0,
                person.compareTo(person));
    }
}
